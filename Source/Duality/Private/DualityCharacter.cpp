// Fill out your copyright notice in the Description page of Project Settings.


#include "DualityCharacter.h"

// Sets default values
ADualityCharacter::ADualityCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADualityCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADualityCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADualityCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

