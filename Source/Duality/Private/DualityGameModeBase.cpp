// Fill out your copyright notice in the Description page of Project Settings.


#include "DualityGameModeBase.h"

#include "ReferencePoint.h"
#include "Kismet/GameplayStatics.h"

void ADualityGameModeBase::StartPlay()
{
	Super::StartPlay();

	// find actor of class ReferencePoint and save them
	TArray<AActor*> ActorsToFind;
	if (const UWorld* World = GetWorld())
	{
		UGameplayStatics::GetAllActorsOfClass(World, AReferencePoint::StaticClass(), ActorsToFind);
	}
	
	for (AActor* ReferencePointActor : ActorsToFind)
	{
		//Is this Actor of type AReferencePoint class?
		AReferencePoint* ReferencePointCast = Cast<AReferencePoint>(ReferencePointActor);
		if (ReferencePointCast)
		{
			if (ReferencePointCast->Id == 1)
			{
				ReferencePoint1 = ReferencePointCast;
			}
			if (ReferencePointCast->Id == 2)
			{
				ReferencePoint2 = ReferencePointCast;
			}
		}
	}
}
