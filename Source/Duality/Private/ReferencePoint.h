// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ReferencePoint.generated.h"

UCLASS()
class AReferencePoint : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AReferencePoint();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// property
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Id;
};
