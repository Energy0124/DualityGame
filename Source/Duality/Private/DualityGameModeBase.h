// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ReferencePoint.h"
#include "GameFramework/GameModeBase.h"
#include "DualityGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ADualityGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	
	virtual void StartPlay() override;

	// property
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AReferencePoint* ReferencePoint1;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AReferencePoint* ReferencePoint2;
};
